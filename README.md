# Universal Sort Engine Demo 1

## The Royal Mail Console Application
* This repo holds an application that demonstrates how to write a Royal Mail High sort
* Royal Mail High sort is the simplest of all to implement
* For brevity all parameters are hard-coded and there are no parameter validation checks
* This is not intended as production ready code but is intended for demonstration purposes
* In our user documentation we frequently abbreviate Universal Sort Engine to USE
* USE supports both MailMark and DSA sort. These require additional parameters and are covered by other demo applications

## Steps Required To Run The Console Application

### Pre-requisite
* You must have git installed and a git client such as Git Bash (install from https://git-scm.com/)

### Step 1 Credentials
* Request NuGet Server credentials and 
* A USE API Product Key

from The Software Bureau Limited at support@thesoftwarebureau.com

### Step 2 Connect Visual Studio to The Software Bureau Nuget Server
* Download latest nuget.exe : https://dist.nuget.org/win-x86-commandline/latest/nuget.exe
* Add TSB as a package feed source from the command line as follows:
	* nuget sources Add -Name "TSB" -Source "https://nuget.thesoftwarebureau.com/nuget"
	* nuget sources Update -Name "TSB" -UserName "UserName" -Password "YOUR_PASSWORD"

![alternativetext](Images/NuGet-Add.png)

* The changes will be saved to your machines configuration file: %appdata%\nuget\NuGet.Config
* Open Visual studio
	* Tools/Options .. NuGet Package Manager
	* You should see TSB listed as a package source

![alternativetext](Images/TSB-As-Packet-Source.png)

### Step 3 Git Pull
* Open Git Bash in your projects directory (install from https://git-scm.com/)
* git clone https://bitbucket.org/thesoftwarebureau/demo.royalmailconsole.git
* or use your favourite git client to clone the RoyalMailConsole solution

![alternativetext](Images/Git-Bash-Clone.png)

### Step 4 Visual Studio
* Open the RoyalMailConsole solution in visual studio
* If you try to build at this stage you will see build errors because the TSB NuGet packages have not been installed
* Select Tools/Nuget Package Manager/Manage NuGet packages for Solution..
* Select TSB as Nuget source from the drop down menu and ensure you have Browse selected at the top of the NuGet window

![alternativetext](Images/Install-TSB-Nuget-Packages.png)

* You should see the following listed among available packages
	* SoftwareBureau.UniversalSortEngine.API
    * SoftwareBureau.UniversalSortEngine.Data.Resource
* Install the latest SoftwareBureau.UniversalSortEngine.API - this will resolve the build errors
* You will need to accept the licence agreement to install the API
* Install the latest SoftwareBureau.UniversalSortEngine.Data.Resource - this will add binary resource files into the solution required by USE
* The project will now build successfully and look like the below

![alternativetext](Images/Final-Project.png)

* If you try to run the project before completing step 3 an error message will be displayed

### Step 5 License
* Launce the application SoftwareBureau.UniversalSortEngine.Activate.exe from the licence folder of the solution
* Press OK in the next window displayed to accept the SystemSettings file

![alternativetext](Images/Settings-File-Accept.png)

* When requested to select Licence Type select "Activation"

![alternativetext](Images/Licence-Type-Activation.png)

* Enter the API Product Key you recieved at Step 1 in the text box and click the Activate button
* Finally copy the following files from licence folder to bin\Debug
	* TurboActivate.dat
    * TurboActivate.dll

### Step 6 Run the Application
* A sample input file "input.csv" was supplied with the console application in the TestData folder
* The application will write the output file and report files to the same TestData folder
* Run the console application

![alternativetext](Images/Run-Console-Application.png)

* Check the following files in the TestData folder

![alternativetext](Images/Output-Files.png)

## What Next?
* Look out for other console application demonstrations - USE supports MailMark and the DSA's


