﻿using System;
using System.IO;
using SoftwareBureau.LimeLicence.TA;
using SoftwareBureau.UniversalSortEngine;
using SoftwareBureau.UniversalSortEngine.MailSort;

namespace RoyalMailConsole
{
    /* Simple demonstration application that performs a Royal Mail sort          */
    /* The API user must ensure the parameters are consistent with carrier rules */
    class Program
    {
        private const string OutputFolder = @"..\..\TestData\";

        /// <summary>
        /// Arguments for a file IO sort
        /// </summary>
        internal class MailSortArgs
        {
            public string InputFile { get; set; }

            public string OutputFile { get; set; }

            public USEParams Params { get; internal set; }

            public IUSESort Sort { get; set; }
        }

        /// <summary>
        /// Provides feedback to the user on what the sort is doing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void UseSort_StatusUpdate(object sender, string e)
        {
            Console.WriteLine(e);
        }

        static void Main(string[] args)
        {
            // create object to hold all parameters required to run a sort
            var fileMailSortArgs = new MailSortArgs
            {
                InputFile = @"..\..\TestData\Input.psv",
                OutputFile = Path.Combine(OutputFolder, "Output.psv")
            };

            // decide the carrier
            const USEParams.Carriers carrier = USEParams.Carriers.RoyalMail;

            // decide sort type required both to create parameters and get correct IUSESortObject
            const USEParams.SortTypes sortType = USEParams.SortTypes.High;

            // set up the parameters
            fileMailSortArgs.Params = new USEParams
            {
                ServiceLevel = USEParams.ServiceLevels.Economy,
                ApplyRegionalSortRegardless = true,
                Carrier = carrier,
                DirectResidueOrder = USEParams.DirectResidueOrders.AlternateDirectResidue,
                SortType = sortType,
                Piece = USEParams.Pieces.Letter,
                MailType = USEParams.MailTypes.Advertising,
                MinimumSelectionSize = 25,
                ItemWeight = 60,
                FixedWeight = true,
                MailingRef = "MailingRef",
                MailCellRef = "MailCellRef"
            };

            // create sort object
            fileMailSortArgs.Sort = new USEMailSort<USEMailSortItem>(@"..\..\db\RMSort.bin");

            // link status messages callbacks
            fileMailSortArgs.Sort.SettingUp += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.SetUpCompleted += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.Sorting += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.SortCompleted += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.Preparing += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.PrepareCompleted += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.ExtraProcessing += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.ExtraProcessingCompleted += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.ReportCreating += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.ReportCreated += UseSort_StatusUpdate;
            fileMailSortArgs.Sort.AddCompleted += UseSort_StatusUpdate;

            // run the sort
            RunSort(fileMailSortArgs);

            // pause and prompt user to exit
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        /// <summary>
        /// Call Sort object Setup, AddRecords, and Prepare to sort the input file
        /// Call Sort.ReadAll() to read the sorted records and write to args.OutputFile
        /// Create Planning Analysis Report, LineListing and BagLabels
        /// </summary>
        /// <param name="args">Arguments required to run a sort with input and output files</param>
        private static void RunSort(MailSortArgs args)
        {
            try
            {
                args.Sort.Setup(args.Params);      // tell sort to prepare for record input
            }
            catch (ProductDetailsException e)
            {
                Console.WriteLine($"Error during sort.Setup with message {e.Message}");
                return;
            }
            catch (USELicenceException e)
            {
                Console.WriteLine($"Error during sort.Setup with message {e.Message}");
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error during sort.Setup {e}");
                return;
            }

            AddRecords(args);                       // add records from Input to the sort object

            try
            {
                args.Sort.Prepare();               // sets up counts, allocation sorts, and does any extra processing, i.e. the main work 
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception during sort prepare {exception}");
                return;
            }

            // individually get each record from sort object and write Output
            using (var fileOut = new FileStream(args.OutputFile, FileMode.Create))
            {
                using (var sw = new StreamWriter(fileOut))
                {
                    var outputRecordNumber = 0;

                    // item is USEMailSortItem in this example
                    // it could also be USEMailSortItemDSA, USEMailSortItemMailmark, USEMailSortItemMailmarkDSA dependent upon the Sort object
                    foreach (var item in args.Sort.ReadAll())
                    {
                        sw.WriteLine(item.ToString());
                        outputRecordNumber += 1;
                        if (outputRecordNumber % 100000 == 0)
                            Console.WriteLine($"Output {outputRecordNumber} records to {args.OutputFile}");
                    }
                    Console.WriteLine($"Output {outputRecordNumber} records to {args.OutputFile}");
                }
            }

            if (args.Sort.SortStatus != SortStatuses.OK)
            {
                Console.WriteLine($"Sort failed with SortStatus {args.Sort.SortStatus}");
                return;
            }

            PlanningAnalysisReport.GeneratePlanningAnalysis(OutputFolder, args.Sort.Report(), args.Params);
            Console.WriteLine("Planning Analysis Report");

            args.Sort.GenerateCSVLineListing(System.IO.Path.Combine(OutputFolder, "LineListing.csv"));
            Console.WriteLine("Line Listing");

            args.Sort.GenerateBagFile(Path.Combine(OutputFolder, "BagLabels.bag"));
            Console.WriteLine("Bag Labels");
        }

        /// <summary>
        /// Input file must be pipe delimited with the following fields and header record A1|A2|A3|A4|A5|A6|A7|Town|Zip|DPS
        /// with optional |Weight
        /// </summary>
        /// <param name="args"></param>
        private static void AddRecords(MailSortArgs args)
        {
            using (var reader = new StreamReader(args.InputFile))
            {
                reader.ReadLine();                       //drop header record
                var record = 0;
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    // <USEMailSortItem> because this is a Royal Mail sort that is not MailMark
                    args.Sort.Add(ConvertToMailSortItem<USEMailSortItem>(record, line));
                    record += 1;
                }
            }
        }

        /// <summary>
        /// Converts the pipe delimited string with required fields to Item object that can be fed into sort object
        /// </summary>
        /// <typeparam name="T">MailSortItem will be USEMailSortItem in this example</typeparam>
        /// <param name="recordNumber">Used to create a sequential "URN"</param>
        /// <param name="pipeDelimitedString">The input pipe delimited record</param>
        /// <returns></returns>
        private static T ConvertToMailSortItem<T>(int recordNumber, string pipeDelimitedString) where T : IUSEable, new()
        {
            var fields = pipeDelimitedString.Split('|');

            if (fields.Length < 11)
                throw new Exception("Input record has less than 11 fields");

            var addressLines = new string[7];
            addressLines[0] = fields[0];
            addressLines[1] = fields[1];
            addressLines[2] = fields[2];
            addressLines[3] = fields[3];
            addressLines[4] = fields[4];
            addressLines[5] = fields[5];
            addressLines[6] = fields[6];

            var item = new T
            {
                URN = recordNumber.ToString(),
                AddLines = addressLines,
                Town = fields[7],
                OutCode = fields[8],
                Incode = fields[9],
                UserDefined = pipeDelimitedString,
                DPS = fields[10]
            };

            if (fields.Length > 11)
                item.Weight = Convert.ToInt32(fields[11]);

            return item;
        }
    }
}

